from functools import reduce
from pickle import REDUCE
from re import L


def main():
    list = [2, 2, 2, 2, 2]

    all_multiplied = reduce(lambda a, b: a * b, list)

    print(all_multiplied)


if __name__ == '__main__':
   main()