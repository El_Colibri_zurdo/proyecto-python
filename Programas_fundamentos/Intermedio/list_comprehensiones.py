def main():
    
    #Primera forma
    #squares = []
    #for i in range (1, 101):
    #    if i % 3 != 0:
    #        squares.append(i**2)

    #Segunda forma
    #Para cada i en el rango de 1 a 101 voy a guardar esa i elvadda al 
    # cuadrada solamente si la i modulo 3 es distinto de 0
    squares = [i**2 for i in range(1, 101) if i % 3 != 0]

    print(squares)


if __name__ == '__main__':
   main()