def main():
    my_list = [1, 'Hello', True, 4.5]
    my_dict = {'firstname': 'Facundo', 'lastname': 'Garcia'}

    super_list = [
        {'firstname': 'Facundo', 'lastname': 'Garcia'},
        {'firstname': 'Miguel', 'lastname': 'Torres'},
        {'firstname': 'Susana', 'lastname': 'Garcia'},
        {'firstname': 'Jose', 'lastname': 'Laureles'},
        {'firstname': 'Luis', 'lastname': 'Fernandes'},
    ]

    super_dict = {

        'natural_nums': [1, 2, 3, 4, 5],
        'integer_nums': [-1, -2, 0, 1, 2],
        'floating_nums': [1.1, 4.5, 6.43]
    }

    for key, value in super_dict.items():
        print (key, ' - ', value)

    for value in super_list:
        for key, value in value.items():
            print (key , ' - ', value)

        


if __name__ == '__main__':
   main()