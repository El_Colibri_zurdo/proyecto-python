
def read():
    numbers = []
    with open('./archivos/numbers.txt', 'r', encoding='utf-8') as f:
        for line in f:
            numbers.append(int(line))
    print(numbers)


def write():
    names = ['Facundo', 'Angel', 'Pepe', 'Pepina', 'Coni']
    with open('./archivos/names.txt', 'w', encoding='utf-8') as f:
        for name in names:
            f.write(name)
            f.write('\n')
    return names

def run():
    read()
    #write()
    names = ['Coque', 'Bella', 'Odi']
    with open('./archivos/names.txt', 'a', encoding='utf-8') as f:
        for name in names:
            f.write(str(name))
            f.write('\n')

def main():
    run()


if __name__ == '__main__':
   main()