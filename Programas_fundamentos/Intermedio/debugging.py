def divisors(num):
    
    divisors = [i for i in range(1, num + 1) if num % i == 0]
    print(divisors)


def main():
    try:    
        num = int(input('Ingresa un num: '))
        if num < 0:
            raise ValueError('Debes ingresar un numero positivo perro')
        print(divisors(num))
        print('Termino')
    except ValueError:
        print('Debes ingresar un numero')

if __name__ == '__main__':
   main()