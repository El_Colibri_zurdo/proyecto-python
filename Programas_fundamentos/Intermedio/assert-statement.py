def divisors(num):
    
    divisors = [i for i in range(1, num + 1) if num % i == 0]
    print(divisors)


def main(): 
        num = input('Ingresa un num: ')
        assert num.isnumeric(), 'Debes ingresar un numero'
        assert int(num) > 0, 'Ingresa un numero positivo mamahuevaso'
        print(divisors(int(num)))
        print('Termino')
        print('Debes ingresar un numero')

if __name__ == '__main__':
   main()