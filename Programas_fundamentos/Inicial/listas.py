def main():

    #Estas son menus 

    #lista = {
        
    #    'llave1' : 1, 
    #    'llave2' : 2,
    #    'llave3' : 3,
    #}
    
    #print(lista['llave1'])
    #print(lista['llave2'])
    #print(lista['llave3'])

    poblacion_paises = {

        #llaves       valor de las llaves
        'Argentina' : 44938712,
        'Brasil' : 210147125,
        'Colombia' : 5037242
    }

    #print(poblacion_paises['Argentina'])

    #Devuelve las llaves
    #for paises in poblacion_paises.keys():
    #    print(paises)

    #devuelve el valor de las llaves
    #for paises in poblacion_paises.values():
    #    print(paises)

    #devuelve el valor de las llaves y las llaves
    for paises, poblacion in poblacion_paises.items():
        print(paises + ' tiene ' + str(poblacion) + ' habitantes')


if __name__ == "__main__":
    main()