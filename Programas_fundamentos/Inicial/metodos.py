#Metodos en python

nombre = input("Dame el nombre bastardo")

#Este metodo pone todos los caracteres del stringe en mayuscula
nombre = nombre.upper()
print(nombre.upper())

#Este metodo pone la primer letra en mayuscula
nombre = nombre.capitalize()
print(nombre.capitalize())

#Este metodo quita los ezpacios vacios
nombre = nombre.strip()
print(nombre.strip())

#Este metodo pone todos los caracteres en minusculas
nombre = nombre.lower()
print(nombre.lower())

#Este metodo nos ayuda a remplazar letras dentro del caracter
nombre = nombre.replace('a', 'o')
print(nombre.replace('a', 'o'))

#Funcion para averiguar el numero de caracteres
print(len(nombre))