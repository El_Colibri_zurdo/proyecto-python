def main():
    my_set = {3, 4, 5}
    print('my_set = ', my_set)

    my_set2 = {'Hola', 23.3, False, True}
    print('my_set = ', my_set2)

    my_set3 = {3, 3, 2}
    print('my_set = ', my_set3)

    #genera erros por intentar meter una lista
    #my_set4 = {[1, 2, 3], 4}
    #print('my_set = ', my_set4)

    #como no tiene datos reconoce emty_set como dict
    empty_set = {}
    print(type(empty_set))
    empty_set = set()
    print(type(empty_set))

    my_list = [1, 1, 2, 3, 4, 4, 5]
    #al guardar una lista dentro del set se convierte a set
    #python nos quita el repetido
    #los set son datos inmutables
    my_set = set(my_list)
    print(my_set)

    my_tuple = ('Hola', 'Hola', 'Hola', 1)
    my_set2 = set(my_tuple)
    #aqui tambien nos quita los repetidos al imprimir
    #y python imprime los set desordenados para ser mas rapido y ahorra memoria
    print(my_set2)

    my_set = {1, 2, 3}
    print('Operaciones con set: ', my_set)

    #Anadir un elemento
    my_set.add(4)
    print('Añadimosun un elemento: ', my_set)

    #Añadimos múltiples elementos
    my_set.update([1, 2, 5])
    print('Añadimos multiples elementos: ', my_set)

    #Anadimos multiples elementos
    my_set.update((1, 7, 2), {6, 8})
    print('Añandimos una tupla y otro set', my_set)

    my_set = {1, 2, 3, 4, 5, 6, 7}
    print('Vamo a borrarle elementos a: ', my_set)

    #Borrar un elemto existente
    my_set.discard(1)
    print(my_set)

    #Borar un elemento existente
    my_set.remove(2)
    print(my_set)

    #Borar un elemento inexistente
    my_set.discard(12)
    print(my_set)

    #Borar un elemento inexistente
    my_set.remove(112)
    print(my_set)

    my_set = {1, 2, 3, 4, 5, 6, 7}
    print('Vamo a borrarle elementos a: ', my_set)

    #Bprra elementos aleatorios
    my_set.pop()
    print(my_set)

    my_set.clear()
    print(my_set)


if __name__ == '__main__':
   main()