from itertools import count
import time

def gen_fibo(max_number:int):

    n1 = 0
    n2 = 1
    counter = 0
    
    #max_number = 500

    #while True < max_number:

    #    if counter == 0:
    #        counter += 1
    #        yield n1
    #    elif counter == 1:
    #        counter += 1
    #        yield n2
    #    else:
    #        aux = n1 + n2

    #        n1, n2 = n2, aux
    #        counter += 1
    #        yield aux

    while n1 <= max_number:

        yield n1
        n1, n2 = n2, n1+n2


if __name__ == '__main__':

    max_number = int(input('Introduce your limit number for the fibonacci\n'))
    fibonacci = gen_fibo(max_number)
    for element in fibonacci:
        print(element)
        time.sleep(0.05)
