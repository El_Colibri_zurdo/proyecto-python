from typing import Dict, List

positives: List[int] = [1,2,3,4,5]

users: Dict[str, int] = {
    'argentinca': 1,
    'mexico': 34,
    'colombia': 45,
}

countries: List[Dict[str, str]] = [
    {
        'name': 'Argentina',
        'people': '45000',
    },
    {
        'name': 'Mexico',
        'people': '9000000',
    },
    {
        'name': 'Colombia',
        'people': '999999999999',
    },
]