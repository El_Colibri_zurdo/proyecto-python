from __future__ import division
from timeit import repeat


def make_division_by(n: int):
    def numerator(int: int):
        assert n != 0, 'you cannot divide by zero'
        return int / n
    return numerator

def main():
    division_by_3 = make_division_by(3)
    print(division_by_3(18))
    division_by_5 = make_division_by(5)
    print(division_by_5(100))
    division_by_18 = make_division_by(18)
    print(division_by_18(54))


if __name__ == '__main__':
   main()